
from urllib import response
from django.shortcuts import render,HttpResponse
import speech_recognition as sr 
import mimetypes
from django.http.response import HttpResponse
import os 
from pathlib import Path
from pydub import AudioSegment
from pydub.silence import split_on_silence
import requests
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status 
from .serializers import UrlSerializer



@api_view(['POST'])
def home(request):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    ser = UrlSerializer(data=request.data)
    
    if ser.is_valid():
        b = ser.data
        a=b["url"]
        r = sr.Recognizer()    # create a speech recognition object
        url_input =  a
        filename = str(url_input).replace('/','-')[7:-4]
        print(filename)
        
        def download_audio(a, filename):
            response = requests.get(a)  
            filepath = str(BASE_DIR) + '/media/' + filename
            with open(filepath, 'wb') as file:   # create the file
                file.write(response.content) # write response contents to the file
            return filepath
        path = download_audio(a, filename)


        def get_large_audio_transcription(path, filename):
            # open the audio file using pydub
            sound = AudioSegment.from_wav(path)  
            # split audio sound where silence is 700 miliseconds or more and get chunks
            chunks = split_on_silence(sound,
                # experiment with this value for your target audio file
                min_silence_len = 500,
                # adjust this per requirement
                silence_thresh = sound.dBFS-14,
                # keep the silence for 1 second, adjustable as well
                keep_silence=500,
            )
            folder_name = "audio-chunks"
            # create a directory to store the audio chunks
            if not os.path.isdir(folder_name):
                os.mkdir(folder_name)
            whole_text = ""
            # process each chunk 
            for i, audio_chunk in enumerate(chunks, start=1):
                # export audio chunk and save it in
                # the `folder_name` directory.
                chunk_filename = os.path.join(folder_name, f"chunk{i}.wav")
                audio_chunk.export(chunk_filename, format="wav")
                # recognize the chunk
                with sr.AudioFile(chunk_filename) as source:
                    audio_listened = r.record(source)
                    # try converting it to text
                    try:
                        text = r.recognize_google(audio_listened, language="fa-IR")
                    except sr.UnknownValueError as e:
                        print("Error:", str(e))
                    else:
                        text = f"{text.capitalize()}. "
                        print(chunk_filename, ":", text)
                        whole_text += text
                    filepath = str(BASE_DIR) + '/media/' + filename + '.txt'
                    with open(filepath, 'w') as file:
                        file.write(whole_text)
        get_large_audio_transcription(path, filename)


        return Response({"massage":"انجام شد"})
    else:
        return Response(ser.errors)


